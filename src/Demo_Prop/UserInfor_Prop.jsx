import React, { Component } from "react";

export default class UserInfor_Prop extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <p>UserInfor_Prop</p>
        <h2>Username: {this.props.username}</h2>
        <h3>Age: {this.props.userAge}</h3>
        <button
          onClick={() => {
            this.props.handleChangeName("Tom");
          }}
          className="btn btn-danger"
        >
          Change name
        </button>
      </div>
    );
  }
}
