import React, { Component } from "react";
import UserInfor_Prop from "./UserInfor_Prop";

export default class Demo_Prop extends Component {
  state = {
    username: "Alice",
    age: 2,
  };
  // handleChangeName = () => {
  //   this.setState({ username: "Bob", age: 10 });
  // };
  handleChangeName = (name) => {
    this.setState({ username: name });
  };
  render() {
    return (
      <div>
        <p>Demo_Prop</p>
        <UserInfor_Prop
          username={this.state.username}
          userAge={this.state.age}
          handleChangeName={this.handleChangeName}
        />
      </div>
    );
  }
}
