import React, { Component } from "react";
import CardItem from "./CardItem";

export default class RenderWithMap extends Component {
  state = {
    phoneArr: [
      {
        name: "Scottish Fold",
        img: "http://loremflickr.com/640/480/technics",
        price: "463.00",
        id: "1",
      },
      {
        name: "American Wirehair",
        img: "http://loremflickr.com/640/480/technics",
        price: "600.00",
        id: "2",
      },
      {
        name: "Singapura",
        img: "http://loremflickr.com/640/480/technics",
        price: "515.00",
        id: "3",
      },
      {
        name: "Korat",
        img: "http://loremflickr.com/640/480/technics",
        price: "819.00",
        id: "4",
      },
      {
        name: "Thai",
        img: "http://loremflickr.com/640/480/technics",
        price: "349.00",
        id: "5",
      },
      {
        name: "Himalayan",
        img: "http://loremflickr.com/640/480/technics",
        price: "660.00",
        id: "6",
      },
      {
        name: "Highlander",
        img: "http://loremflickr.com/640/480/technics",
        price: "516.00",
        id: "7",
      },
    ],
  };
  render() {
    return (
      <div>
        <p>RenderWithMap</p>
        <div className="row">
          {this.state.phoneArr.map((item, index) => {
            return <CardItem data={item} key={index.toString() + item.id} />;
          })}
        </div>
      </div>
    );
  }
}
