import React, { Component } from "react";

export default class demo extends Component {
  state = {
    phoneArr: [
      {
        name: "Scottish Fold",
        img: "http://loremflickr.com/640/480/technics",
        price: "463.00",
        id: "1",
      },
      {
        name: "American Wirehair",
        img: "http://loremflickr.com/640/480/technics",
        price: "600.00",
        id: "2",
      },
      {
        name: "Singapura",
        img: "http://loremflickr.com/640/480/technics",
        price: "515.00",
        id: "3",
      },
      {
        name: "Korat",
        img: "http://loremflickr.com/640/480/technics",
        price: "819.00",
        id: "4",
      },
      {
        name: "Thai",
        img: "http://loremflickr.com/640/480/technics",
        price: "349.00",
        id: "5",
      },
      {
        name: "Himalayan",
        img: "http://loremflickr.com/640/480/technics",
        price: "660.00",
        id: "6",
      },

      {
        name: "Highlander",
        img: "http://loremflickr.com/640/480/technics",
        price: "516.00",
        id: "7",
      },
      {
        name: "American Curl",
        img: "http://loremflickr.com/640/480/technics",
        price: "403.00",
        id: "8",
      },
    ],
  };
  render() {
    return (
      <div>
        <p>RenderWithMap</p>
        <div className="row">
          {this.state.phoneArr.map((item, index) => {
            return (
              <div
                key={index.toString() + item.id}
                className="card col-3"
                style={{ width: "18rem" }}
              >
                <img
                  className="card-img-top"
                  src={item.img}
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h5 className="card-title">{item.name}</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    {item.price}
                  </a>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
