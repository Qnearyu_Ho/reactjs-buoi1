import React, { Component } from "react";
import styles from "./ex_layout.module.css";
export default class HeaderComponent extends Component {
  render() {
    return (
      <div className={`${styles["header"]}`}>
        <div className={`${styles["start_B"]}`}>Start Bootstrap</div>
        <div className={`${styles["nav-links"]}`}>
          <ul>
            <li>
              <a className="active" href="#">
                Home
              </a>
            </li>
            <li>
              <a href="#">About</a>
            </li>
            <li>
              <a href="#">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
