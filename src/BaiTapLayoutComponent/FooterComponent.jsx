import React, { Component } from "react";
import styles from "./ex_layout.module.css";
export default class FooterComponent extends Component {
  render() {
    return (
      <div className={`${styles["footer_content"]}`}>
        Copyright © Your Website 2022
      </div>
    );
  }
}
