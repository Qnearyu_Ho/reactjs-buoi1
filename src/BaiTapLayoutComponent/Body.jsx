import React, { Component } from "react";

import styles from "./ex_layout.module.css";
export default class Body extends Component {
  render() {
    return (
      <div className={`${styles["body_content"]}`}>
        <h1 className={`${styles["title-body"]}`}>A warm welcome!</h1>
        <p>
          Bootstrap utility classes are used to create this jumbotron since the
          old
          <br /> component has been removed from the framework. <br />
          Why create custom CSS when you can use utilities?
        </p>
        <button className="btn btn-primary">Call to action</button>
      </div>
    );
  }
}
