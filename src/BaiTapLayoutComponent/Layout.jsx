import React, { Component } from "react";
import HeaderComponent from "./HeaderComponent";
import Body from "./Body";
import FooterComponent from "./FooterComponent";

import styles from "./ex_layout.module.css";

export default class Layout extends Component {
  render() {
    return (
      <div className="">
        <HeaderComponent />
        <Body />

        <FooterComponent />
      </div>
    );
  }
}
