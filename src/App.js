// import logo from './logo.svg';

import "./App.css";
import DemoClass from "./demo_Component/DemoClass";
import DemoFunction from "./demo_Component/DemoFunction";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import Data_Binding from "./Data_Binding/Data_Binding";
import Event_Binding from "./Event_Binding/Event_Binding";
import Conditional_Rendering from "./Conditional_Rendering/Conditional_Rendering";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import Demo_Prop from "./Demo_Prop/Demo_Prop";
import CardItem from "./RenderWithMap/CardItem";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import Layout from "./BaiTapLayoutComponent/Layout";

function App() {
  return (
    <div className="App">
      {/* <DemoClass></DemoClass> */}

      {/* <DemoFunction /> */}
      {/* <Ex_Layout /> */}
      {/* <Data_Binding /> */}
      {/* <Event_Binding/> */}
      {/* <Conditional_Rendering/> */}
      {/* <RenderWithMap/> */}
      {/* <CardItem/> */}
      {/* <Demo_Prop/> */}
      {/* <Ex_ShoeShop/> */}
      <Layout/>
    </div>
  );
}

export default App;
