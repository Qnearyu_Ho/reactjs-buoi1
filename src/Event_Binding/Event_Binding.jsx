import React, { Component } from "react";

export default class Event_Binding extends Component {
  handleClickMe = () => {
    console.log("Yes");
  };
  handleSayHello = (name) => {
    console.log(name);
  };
  render() {
    return (
      <div>
        <p>Event_Binding</p>
        <button onClick={this.handleClickMe} className="btn btn-success">
          Click me
        </button>
        <button
          onClick={() => {
            this.handleSayHello("Alice");
          }}
          className="btn btn-secondary"
        >
          Click to say hello
        </button>
      </div>
    );
  }
}
