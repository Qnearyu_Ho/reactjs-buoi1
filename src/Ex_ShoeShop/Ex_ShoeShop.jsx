import React, { Component } from "react";
import { dataShoe } from "./data_shoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detailShoe: dataShoe[0],
    gioHang: [],
  };
  handleXemChiTiet = (idShoe) => {
    // c1
    let index = this.state.shoeArr.findIndex((item) => {
      return item.id == idShoe;
    });

    index !== -1 &&
      this.setState({
        detailShoe: this.state.shoeArr[index],
      });
  };
  handleAddToCart = (shoe) => {
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang.push(shoe);
    this.setState({
      gioHang: cloneGioHang,
    });
    console.log(this.state.gioHang.length);
  };
  render() {
    return (
      <div>
        <Cart />
        <ListShoe
          data={this.state.shoeArr}
          handleXemChiTiet={this.handleXemChiTiet}
          handleAddtoCart={this.handleAddToCart}
        />
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
